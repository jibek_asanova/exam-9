import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addNewContact} from "../../store/actions/contactsActions";
import {useHistory} from "react-router-dom";
import Form from "../../components/Form/Form";
import Spinner from "../../components/UI/Spinner/Spinner";

const AddContact = () => {
    const history = useHistory()
    const dispatch = useDispatch();
    const loading = useSelector(state => state.loading);

    const [contact, setContact] = useState( {
        name: '',
        phone: '',
        email: '',
        photo: ''
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setContact(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const addContact = async e => {
        e.preventDefault();
        await dispatch(addNewContact(contact));
        history.replace('/');
    };

    let form = (
        <Form
            addContact={addContact}
            name={contact.name}
            phone={contact.phone}
            email={contact.email}
            photo={contact.photo}
            onInputChange={onInputChange}
        />
    );

    if(loading) {
        form = <Spinner/>
    }

    return (
        <div>
            <h2>Add New Contact</h2>
            {form}
        </div>
    );
};

export default AddContact;