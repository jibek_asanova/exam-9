import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {editContact as edit, editedContactGetRequest} from "../../store/actions/contactsActions";
import Form from "../../components/Form/Form";
import Spinner from "../../components/UI/Spinner/Spinner";

const EditContact = ({match}) => {
    const history = useHistory();
    const dispatch = useDispatch();
    const matchId = match.params.id;
    const editContactsList = useSelector(state => state.editContactsList);
    const loading = useSelector(state => state.loading);

    const [contact, setContact] = useState( {
        name: '',
        phone: '',
        email: '',
        photo: ''
    });

    useEffect(() => {
        dispatch(editedContactGetRequest(matchId));
        setContact(prev => ({
            ...prev,
            name: editContactsList.name,
            phone: editContactsList.phone,
            email: editContactsList.email,
            photo: editContactsList.photo,
        }))
    }, [
        dispatch,
        matchId,
        editContactsList.name,
        editContactsList.phone,
        editContactsList.email,
        editContactsList.photo
    ]);

    const onInputChange = e => {
        const {name, value} = e.target;

        setContact(prev => ({
            ...prev,
            [name]: value
        }))
    };

    const editContact = async (e, id) => {
        e.preventDefault();
        await dispatch(edit(contact, id));
        history.replace('/');
    };

    let form = (
        <Form
            addContact={(e) => editContact(e, matchId)}
            name={contact.name}
            phone={contact.phone}
            email={contact.email}
            photo={contact.photo}
            onInputChange={onInputChange}
        />
    )

    if(loading) {
        form = <Spinner/>
    }

    return (
        <div>
            <h2>Edit Contact</h2>
            {form}
        </div>
    );
};

export default EditContact;