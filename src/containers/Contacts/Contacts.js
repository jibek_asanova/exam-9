import React, {useEffect, useState} from 'react';
import {Link, useHistory} from "react-router-dom";
import './Contacts.css';
import {useDispatch, useSelector} from "react-redux";
import {deleteContact, getContactData, getContacts} from "../../store/actions/contactsActions";
import Modal from "../../components/UI/Modal/Modal";
import ModalContent from "../../components/ModalContent/ModalContent";
import noPhotoAvatar from "../../components/UI/assets/images.png";

const Contacts = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const contactsList = useSelector(state => state.contactsList);
    const contact = useSelector(state => state.contact);
    const [id, setId] = useState(null);
    const [open, setOpen] = useState(false);

    const purchaseHandler = () => {
        setOpen(!open);
    };

    const getContactInfo = async (id) => {
        setOpen(!open);
        await dispatch(getContactData(id));
        setId(id);
    };


    useEffect(() => {
        dispatch(getContacts())
    }, [dispatch]);

    const removeContact = (id) => {
        dispatch(deleteContact(id));
        setOpen(!open);
        history.replace('/');
    };

    const editContact = (id) => {
        history.replace('/edit-contact/' + id);
    };

    return (
        <div>
            <div className="title-block">
                <h2>Contacts</h2>
                <Link to="/add-contact" className="btn btn-primary">Add New Contact</Link>
            </div>
            <Modal
                show={open}
                closed={purchaseHandler}
            >
                <ModalContent
                    closed={purchaseHandler}
                    photo={contact.photo}
                    name={contact.name}
                    phone={contact.phone}
                    email={contact.email}
                    remove={() => removeContact(id)}
                    edit={() => editContact(id)}
                />
            </Modal>
            {contactsList.map(contact => (
                <div key={contact.id}
                     id={contact.id}
                     className="ContactsList"
                     onClick={() => getContactInfo(contact.id)}>
                    {contact.photo ? (<img src={contact.photo} alt='itemImg' width='100px' className="click"/>) :
                        (<img src={noPhotoAvatar} alt='no' className='avatar'/>)}
                    <p>{contact.name}</p>
                </div>
            ))}
        </div>
    );
};

export default Contacts;