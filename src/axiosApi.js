import axios from "axios";

const axiosApi = axios.create({
    baseURL : 'https://asanova-jibek-exam-9-default-rtdb.firebaseio.com/'
});

export default axiosApi;