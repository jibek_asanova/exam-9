import './App.css';
import {Route, Switch} from "react-router-dom";
import Contacts from "./containers/Contacts/Contacts";
import AddContact from "./containers/AddContact/AddContact";
import Layout from "./components/Layout/Layout";
import EditContact from "./containers/EditContact/EditContact";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Contacts}/>
            <Route path="/contacts" exact component={Contacts}/>
            <Route path="/add-contact" exact component={AddContact}/>
            <Route path="/edit-contact/:id" exact component={EditContact}/>
        </Switch>
    </Layout>

);

export default App;
