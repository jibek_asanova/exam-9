import {
    ADD_CONTACT_FAILURE,
    ADD_CONTACT_REQUEST,
    ADD_CONTACT_SUCCESS,
    DELETE_CONTACT_FAILURE,
    DELETE_CONTACT_REQUEST,
    DELETE_CONTACT_SUCCESS, EDIT_CONTACT_FAILURE,
    EDIT_CONTACT_REQUEST, EDIT_CONTACT_SUCCESS,
    FETCH_EDIT_CONTACTS_REQUEST,
    FETCH_EDIT_CONTACTS_SUCCESS,
    GET_CONTACT_INFO_REQUEST,
    GET_CONTACT_INFO_SUCCESS,
    GET_CONTACTS_FAILURE,
    GET_CONTACTS_REQUEST,
    GET_CONTACTS_SUCCESS,
} from "../actions/contactsActions";

const initialState = {
    contactsList: [],
    loading: false,
    error: null,
    contact: [],
    editContactsList: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_CONTACT_REQUEST:
            return {...state, error: null, loading: true}
        case ADD_CONTACT_SUCCESS:
            return {...state, loading: false}
        case ADD_CONTACT_FAILURE:
            return {...state, loading: false, error: action.payload}
        case GET_CONTACTS_REQUEST:
            return {...state, loading: true}
        case GET_CONTACTS_SUCCESS:
            if (action.payload === null) {
                return {...state, loading: false, contactsList: []}
            } else {
                return {...state, loading: false, contactsList: Object.keys(action.payload).map(contact => ({
                        id: contact,
                        ...action.payload[contact]
                    }))};
            }
        case GET_CONTACTS_FAILURE:
            return {...state, loading: false, error: action.payload}
        case GET_CONTACT_INFO_REQUEST:
            return {...state, loading: true}
        case GET_CONTACT_INFO_SUCCESS:
            return {...state, loading: false, contact: action.payload}
        case DELETE_CONTACT_REQUEST:
            return {...state, loading: true};
        case DELETE_CONTACT_SUCCESS:
            return {...state, loading: false};
        case DELETE_CONTACT_FAILURE:
            return {...state, loading: false}
        case FETCH_EDIT_CONTACTS_REQUEST:
            return {...state, loading: true}
        case FETCH_EDIT_CONTACTS_SUCCESS:
            return {...state, loading: false, editContactsList: action.payload}
        case EDIT_CONTACT_REQUEST:
            return {...state, loading: true};
        case EDIT_CONTACT_SUCCESS:
            return {...state, loading: false};
        case EDIT_CONTACT_FAILURE:
            return {...state, loading: false};
        default:
            return state;
    }
}

export default reducer;