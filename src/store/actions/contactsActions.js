import axiosApi from "../../axiosApi";

export const ADD_CONTACT_REQUEST = 'ADD_CONTACT_REQUEST';
export const ADD_CONTACT_SUCCESS = 'ADD_CONTACT_SUCCESS';
export const ADD_CONTACT_FAILURE = 'ADD_CONTACT_FAILURE';

export const GET_CONTACTS_REQUEST = 'GET_CONTACTS_REQUEST';
export const GET_CONTACTS_SUCCESS = 'GET_CONTACTS_SUCCESS';
export const GET_CONTACTS_FAILURE = 'GET_CONTACTS_FAILURE';

export const GET_CONTACT_INFO_REQUEST = 'GET_CONTACT_INFO_REQUEST';
export const GET_CONTACT_INFO_SUCCESS = 'GET_CONTACT_INFO_SUCCESS';
export const GET_CONTACT_INFO_FAILURE = 'GET_CONTACT_INFO_FAILURE';

export const DELETE_CONTACT_REQUEST = 'DELETE_CONTACT_REQUEST';
export const DELETE_CONTACT_SUCCESS = 'DELETE_CONTACT_SUCCESS';
export const DELETE_CONTACT_FAILURE = 'DELETE_CONTACT_FAILURE';

export const EDIT_CONTACT_REQUEST = 'EDIT_CONTACT_REQUEST';
export const EDIT_CONTACT_SUCCESS = 'EDIT_CONTACT_SUCCESS';
export const EDIT_CONTACT_FAILURE = 'EDIT_CONTACT_FAILURE';

export const FETCH_EDIT_CONTACTS_REQUEST = 'FETCH_EDIT_CONTACTS_REQUEST';
export const FETCH_EDIT_CONTACTS_SUCCESS = 'FETCH_EDIT_CONTACTS_SUCCESS';
export const FETCH_EDIT_CONTACTS_FAILURE = 'FETCH_EDIT_CONTACTS_FAILURE';

export const addContactRequest = () => ({type: ADD_CONTACT_REQUEST});
export const addContactSuccess = contact => ({type: ADD_CONTACT_SUCCESS, payload: contact});
export const addContactFailure = () => ({type: ADD_CONTACT_FAILURE});

export const getContactsRequest = () => ({type: GET_CONTACTS_REQUEST});
export const getContactsSuccess = contacts => ({type: GET_CONTACTS_SUCCESS, payload: contacts});
export const getContactsFailure = () => ({type: GET_CONTACTS_FAILURE});

export const getContactInfoRequest = () => ({type: GET_CONTACT_INFO_REQUEST});
export const getContactInfoSuccess = contacts => ({type: GET_CONTACT_INFO_SUCCESS, payload: contacts});
export const getContactInfoFailure = () => ({type: GET_CONTACT_INFO_FAILURE});

export const deleteContactRequest = () => ({type: DELETE_CONTACT_REQUEST});
export const deleteContactSuccess = contact => ({type: DELETE_CONTACT_SUCCESS, payload: contact});
export const deleteContactFailure = () => ({type: DELETE_CONTACT_FAILURE});

export const editContactRequest = () => ({type: EDIT_CONTACT_REQUEST});
export const editContactSuccess = contact => ({type: EDIT_CONTACT_SUCCESS, payload: contact});
export const editContactFailure = () => ({type: EDIT_CONTACT_FAILURE});

export const fetchEditContactsRequest = () => ({type: FETCH_EDIT_CONTACTS_REQUEST});
export const fetchEditContactsSuccess = contacts => ({type: FETCH_EDIT_CONTACTS_SUCCESS, payload: contacts});
export const fetchEditContactsFailure = () => ({type: FETCH_EDIT_CONTACTS_FAILURE});



export const addNewContact = contactData => {
    return async dispatch => {
        try {
            dispatch(addContactRequest());
            await axiosApi.post('/contacts.json', contactData);
            dispatch(addContactSuccess());
        } catch (error) {
            dispatch(addContactFailure(error));
            throw error;
        }
    }
};

export const getContacts = () => {
    return async (dispatch) => {
        dispatch(getContactsRequest());
        try {
            const response = await axiosApi.get('/contacts.json');
            const contactList = response.data;
            dispatch(getContactsSuccess(contactList));
        } catch (error) {
            dispatch(getContactsFailure(error));
        }
    }
};

export const getContactData = (id) => {
    return async (dispatch) => {
        dispatch(getContactInfoRequest());
        try {
            const response = await axiosApi.get('/contacts/' + id + '.json');
            dispatch(getContactInfoSuccess(response.data));
        } catch(error) {
            dispatch(getContactInfoFailure(error));
        }
    }
};

export const deleteContact = (id) => {
    return async (dispatch) => {
        dispatch(deleteContactRequest());
        try {
            const response = await axiosApi.delete('/contacts/' + id + '.json');
            dispatch(deleteContactSuccess(response.data));
            dispatch(getContacts());
        } catch(error) {
            dispatch(deleteContactFailure(error));
        }
    }
};

export const editedContactGetRequest = (id) => {
    return async (dispatch) => {
        dispatch(fetchEditContactsRequest());
        try {
            const response = await axiosApi.get('/contacts/' + id + '.json');
            dispatch(fetchEditContactsSuccess(response.data));
        } catch(error) {
            dispatch(fetchEditContactsFailure(error));
        }
    }
};


export const editContact = (contactData, id) => {
    return async(dispatch) => {
        dispatch(editContactRequest());
        try {
            await axiosApi.put('/contacts/' + id + '.json', contactData);
            dispatch(editContactSuccess());
        } catch (error) {
            dispatch(editContactFailure());
        }
    }
};