import React from 'react';
import {Link, NavLink} from "react-router-dom";
import phone from '../UI/assets/phone-book.png';

const Navbar = () => {
    return (
        <div className="MenuBar">
            <nav className="navbar navbar-light" style={{backgroundColor: '#e3f2fd'}}>
                <nav className="navbar navbar-expand-lg navbar-light ">
                    <div className="container-fluid">
                        <Link to="/"><img src={phone} alt="pizza-logo" width="40px" className="Logo"/></Link>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"/>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div className="navbar-nav">
                                <NavLink to="/contacts" className="nav-link" >Contact List</NavLink>
                            </div>
                        </div>
                    </div>
                </nav>
            </nav>
        </div>

    );
};

export default Navbar;