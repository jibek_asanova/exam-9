import React from 'react';
import './ModalContent.css';
import phoneIcon from '../UI/assets/telephone.png';
import emailIcon from '../UI/assets/envelope.png';
import noPhotoAvatar from "../UI/assets/images.png";

const ModalContent = props => {
    return(
        <div className="modal-container">
            <span className="close-button" onClick={props.closed}>X</span>

            <div className="modalInfo">
                {props.photo ?
                    (<img src={props.photo} alt='Preview' className='avatar'/>) :
                    (<img src={noPhotoAvatar} alt='no' className='avatar'/>)}

                <div className="about-contact">
                    <h1>{props.name}</h1>
                    <p><img src={phoneIcon} alt="phoneIcon" width="5px" className="icon"/>{props.phone}</p>
                    <p><img src={emailIcon} alt="phoneIcon" width="5px" className="icon"/>{props.email}</p>
                    <button onClick={props.edit} className="btn btn-success">Edit</button>
                    <button onClick={props.remove} className="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    )
};

export default ModalContent;