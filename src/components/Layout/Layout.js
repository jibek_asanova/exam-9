import React from 'react';
import './Layout.css';
import Navbar from "../Navbar/Navbar";

const Layout = ({children}) => {
    return (
        <>
            <Navbar/>
            <main className="container">
                {children}
            </main>
        </>
    );
};

export default Layout;