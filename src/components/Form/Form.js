import React from 'react';
import './Form.css';
import noPhotoAvatar from '../UI/assets/images.png';
const Form = props => {
    return (
        <form className="Form" onSubmit={props.addContact}>
            <input
                className="Input"
                type="text"
                name="name"
                placeholder="Name"
                value={props.name}
                onChange={props.onInputChange}
            />
            <input
                className="Input"
                type="text"
                name="phone"
                placeholder="Phone"
                value={props.phone}
                onChange={props.onInputChange}
            />
            <input
                className="Input"
                type="email"
                name="email"
                placeholder="Email"
                value={props.email}
                onChange={props.onInputChange}
            />
            <input
                className="Input"
                type="text"
                name="photo"
                placeholder="Photo"
                value={props.photo}
                onChange={props.onInputChange}
            />
            <div>{props.photo ?
                (<img src={props.photo} alt='Preview' className='avatar'/>) :
                (<img src={noPhotoAvatar} alt='no' className='avatar'/>)
            }</div>
            <button type="submit" className="btn btn-primary">Save</button>
        </form>
    );
};

export default Form;